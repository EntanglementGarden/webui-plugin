// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package plugin

import (
	"context"
	"errors"
	"fmt"
	"net"
	"os"
	"strings"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	"entanglement.garden/webui-plugin/plugin_protos"
	"entanglement.garden/webui-plugin/templates"
)

type Plugin struct {
	plugin_protos.UnimplementedPluginServiceServer

	Name      string
	Extension string

	Endpoints map[string]map[string]Endpoint // map[path]map[method]Endpoint
	Menu      *plugin_protos.MenuItem
}

type Error struct {
	Err         error
	ErrTitle    string
	Description string
}

func (e Error) Error() string {
	if e.Err == nil {
		return ""
	}
	return e.Err.Error()
}

type Endpoint func(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error)

type MenuItem struct {
	Name  string
	Path  string
	Items []MenuItem
}

func (p *Plugin) ListenAndServe() {
	socketFile := os.Args[1]

	if _, err := os.Stat(socketFile); !os.IsNotExist(err) {
		if err := os.RemoveAll(socketFile); err != nil {
			log.Fatal(err)
		}
	}

	listener, err := net.Listen("unix", socketFile)
	if err != nil {
		log.Error("grpc server failed to listen: ", err)
		return
	}
	defer func() {
		log.Debug("cleaning up ", socketFile)
		if err := listener.Close(); err != nil {
			log.Warn("error closing socket: ", err)
		}

		if err := os.Remove(socketFile); err != nil {
			log.Warn("error cleaning up socket file ", socketFile, ": ", err)
		}
	}()

	log.Debug("Listening on ", socketFile)

	server := grpc.NewServer(grpc.ChainUnaryInterceptor(loggingInterceptor))
	plugin_protos.RegisterPluginServiceServer(server, p)

	err = server.Serve(listener)
	if err != nil {
		log.Fatal("error from grpc listener: ", err)
	}
}

func (p Plugin) Settings(ctx context.Context, _ *plugin_protos.Empty) (*plugin_protos.Plugin, error) {
	paths := []string{}
	for path := range p.Endpoints {
		paths = append(paths, path)
	}

	menuItems := []*plugin_protos.MenuItem{}
	if p.Menu != nil {
		menuItems = append(menuItems, p.Menu)
	}

	return &plugin_protos.Plugin{
		Name:      p.Name,
		Extension: p.Extension,
		Paths:     paths,
		Menus:     menuItems,
	}, nil
}

func (p Plugin) HandleRequest(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
	handler, ok := p.Endpoints[request.Route][request.Method]
	if !ok {
		return nil, errors.New("route not found")
	}

	ctx = context.WithValue(ctx, authTokenContextKey, request.AuthToken)

	resp, err := handler(ctx, request)
	if err != nil {
		resp = &plugin_protos.Response{StatusCode: 500, Title: "Internal Server Error"}

		var template templates.Error
		if pluginError, ok := err.(Error); ok {
			template = templates.Error{
				Error:       pluginError.Err,
				ErrTitle:    pluginError.ErrTitle,
				Description: pluginError.Description,
			}
		} else {
			template = templates.Error{Error: err}
		}
		resp.Body = template.Body()
	}

	if strings.HasPrefix(resp.Headers["Location"], "/") {
		resp.Headers["Location"] = fmt.Sprintf("/%s%s", p.Extension, resp.Headers["Location"])
	}

	return resp, nil
}

func loggingInterceptor(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	resp, err = handler(ctx, req)
	if err != nil {
		log.WithField("grpc_method", info.FullMethod).Warn("error from handler: ", err)
		return resp, err
	}

	return resp, err
}

module entanglement.garden/webui-plugin

go 1.19

require (
	entanglement.garden/api-client v0.0.0-20231130195305-6be5db796d49
	github.com/sirupsen/logrus v1.9.3
	github.com/valyala/quicktemplate v1.7.0
	google.golang.org/grpc v1.55.0
	google.golang.org/protobuf v1.30.0
)

require (
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	golang.org/x/net v0.12.0 // indirect
	golang.org/x/sys v0.13.0 // indirect
	golang.org/x/text v0.13.0 // indirect
	google.golang.org/genproto v0.0.0-20230306155012-7f2fa6fef1f4 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

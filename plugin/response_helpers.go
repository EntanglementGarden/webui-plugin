// Copyright Entanglement Garden Developers
// SPDX-License-Identifier: AGPL-3.0-only

package plugin

import (
	"context"
	"io"
	"net/http"

	"github.com/valyala/quicktemplate"

	"entanglement.garden/webui-plugin/plugin_protos"
)

type Page interface {
	Title() string
	StreamTitle(*quicktemplate.Writer)
	WriteTitle(io.Writer)
	Body() string
	StreamBody(*quicktemplate.Writer)
	WriteBody(io.Writer)
}

func WritePage(ctx context.Context, request *plugin_protos.Request, page Page) (*plugin_protos.Response, error) {
	return &plugin_protos.Response{
		StatusCode: 200,
		Headers: map[string]string{
			"Content-Type": "text/html; charset=utf-8",
		},
		Title: page.Title(),
		Body:  page.Body(),
	}, nil
}

func Redirect(path string) (*plugin_protos.Response, error) {
	return &plugin_protos.Response{
		StatusCode: http.StatusFound,
		Headers:    map[string]string{"Location": path},
		Raw:        true,
	}, nil
}

func AlwaysRedirect(path string) Endpoint {
	return func(ctx context.Context, request *plugin_protos.Request) (*plugin_protos.Response, error) {
		return &plugin_protos.Response{
			StatusCode: 302,
			Headers:    map[string]string{"Location": path},
			Raw:        true,
		}, nil
	}
}

# Entanglement WebUI Plugin

This module contains helpers to write Entanglement WebUI Plugins (EWPs). Generally these are a second `main` package within the module:

`web/main.go`:

```go
package main

import (
    "github.com/sirupsen/logrus"

	"entanglement.garden/webui-plugin/plugin"
	"entanglement.garden/webui-plugin/plugin_protos"
)

var p = plugin.Plugin{
	Name: "Example Entanglement Extension",
	Slug: "example",
	Endpoints: map[string]map[string]plugin.Endpoint{
		"/": {
			"GET": IndexPage,
		},
		"/whatever": {
			"GET": WhateverPage,
            "POST": CreateWhatever,
		},
	},
	Menu: &plugin_protos.MenuItem{
		Name: "Example",
		Path: "/example",
		Subitems: []*plugin_protos.MenuItem{
			{
				Name: "Whatever",
				Path: "/whatever",
			},
		},
	},
}

func main() {
	p.ListenAndServe()
}
```

Then implement the endpoint functions defined in Endpoints. For examples, check most of the core Entanglement extensions
